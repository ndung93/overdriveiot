    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <div class="footer-info">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-12">
                        <div class="row">
                            <div class="col-6 col-md-3">
                                <div class="footer-menu">
                                    <h4>Company</h4>
                                    <ul class="hidden-md hidden-sm">
                                        <li><a href="https://overdriveiot.com/">Home</a></li>
                                        <li><a href="https://overdriveiot.com/about/">About</a></li>
                                        <li><a href="https://overdriveiot.com/contact/">Contact</a></li>
                                        <li><a href="https://overdriveiot.com/careers/">Career</a></li>
                                    </ul>
                                    <ul class="hidden-xs">
                                        <li><a href="https://overdriveiot.com/">Home</a></li>
                                        <li><a href="https://overdriveiot.com/about/">About</a></li>
                                        <li><a href="https://overdriveiot.com/knowledge/news/">Knowledge</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="footer-menu">
                                    <h4>Services</h4>
                                    <ul class="hidden-md hidden-sm">
                                        <li><a href="https://overdriveiot.com/products/overwheels/">OverWheels</a></li>
                                        <li><a href="https://overdriveiot.com/products/overpax/">OverPax</a></li>
                                        <li><a href="https://overdriveiot.com/products/overguard/">OverGuard</a></li>
                                        <li><a href="https://overdriveiot.com/solutions/">Solutions</a></li>
                                        <li><a href="https://overdriveiot.com/SEM/campaign/">Request Demo</a></li>
                                    </ul>
                                    <ul class="hidden-xs">
                                        <li><a href="https://overdriveiot.com/solutions/">Solutions</a></li>
                                        <li><a href="https://overdriveiot.com/products/">Products</a></li>
                                        <li><a href="https://overdriveiot.com/programmes/">Programmes</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="footer-menu">
                                    <h4 class="hidden-xs">Terms</h4>
                                    <h4 class="hidden-md hidden-sm">Resources</h4>
                                    <ul class="hidden-md hidden-sm">
                                        <li><a href="">Brochures</a></li>
                                        <li><a href="">Blog</a></li>
                                        <li><a href="https://overdriveiot.com/knowledge/news/">News</a></li>
                                        <li><a href="">FAQ</a></li>
                                        <li><a href="">Sitemap</a></li>
                                    </ul>
                                    <ul class="hidden-xs">
                                        <li><a href="https://overdriveiot.com/terms-conditions/">Terms & Conditions</a></li>
                                        <li><a href="https://overdriveiot.com/privacy-policy/">Privacy Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="footer-menu mb-bt-0">
                                    <h4 class="hidden-xs">Contact</h4>
                                    <h4 class="hidden-md hidden-sm">Terms</h4>
                                    <ul class="hidden-md hidden-sm">
                                        <li><a href="https://overdriveiot.com/terms-conditions/">Terms & Conditions</a></li>
                                        <li><a href="https://overdriveiot.com/privacy-policy/">Privacy Policy</a></li>
                                    </ul>
                                    <ul class="hidden-xs">
                                        <li><a href="https://overdriveiot.com/careers/">Careers</a></li>
                                        <li><a href="https://overdriveiot.com/contact/">Contact</a></li>
                                    </ul>
                                </div>
                                <div class="social">
                                    <ul>
                                        <li>
                                            <a target="_blank" href="https://www.facebook.com/overdrivenow">
                                                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/facebook.png" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="https://www.linkedin.com/company/overdrive-iot-pte-ltd">
                                                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/in.png" alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1">
                        <div class="footer-logo">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/footer-logo.png" alt="">
                        </div>
                        <div class="company-contact">
                            <p>Overdrive IOT Pte Ltd <br>100E Pasir Panjang Road <br>#04-01 Century Warehouse <br>Singapore 118521</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>Overdrive © 2021 Overdrive IOT Pte Ltd. <br class="hidden-md">All rights reserved.</p>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- Js -->
    <script defer src="<?php echo bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
    <script defer src="<?php echo bloginfo('template_url'); ?>/assets/js/slick.min.js"></script>
    <script defer src="<?php echo bloginfo('template_url'); ?>/assets/js/sticky.min.js"></script>
    <script defer src="<?php echo bloginfo('template_url'); ?>/assets/js/wow.min.js"></script>
    <script src="<?php echo bloginfo('template_url'); ?>/assets/js/main.js"></script>
    <?php wp_footer(); ?>
    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-97562784-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-97562784-1');
</script>

</body>
</html>