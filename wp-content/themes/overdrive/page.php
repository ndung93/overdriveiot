<?php get_header(); ?>
<!-- Banner -->
<section class="banner hidden-xs hidden-sm">
    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/banner.jpg" class="main-banner">
    <div class="container banner-content-wrapp">
        <div class="row">
            <div class="col-md-7">
                <div class="banner-content-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h2>A Full Fledge <br>Fleet Management System</h2>
                    <p>Navigate with ease with Overdrive’s industry-leading vehicle <br>tracking system.  More than a car GPS tracker, we deliver all the <br>valuable data you need to get moving efficiently.</p>
                </div>
            </div>
            <div class="col-md-5">
                <div class="banner-form wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h4 class="text-left">Get a Free Demo</h4>
<!--                    <div class="form-contact">-->
<!--                        <div class="form-group">-->
<!--                            <input type="text" class="form-control" name="fname" id="fname" placeholder="Name*">-->
<!--                        </div>-->
<!--                        <div class="form-group">-->
<!--                            <input type="email" class="form-control" name="email" id="email" placeholder="Email*">-->
<!--                        </div>-->
<!--                        <div class="form-group c-row">-->
<!--                            <div class="c-col-1">-->
<!--                                <select name="country" id="country" class="form-control">-->
<!--                                    <option value="65">+65</option>-->
<!--                                    <option value="84">+84</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <div class="c-col-2">-->
<!--                                <input type="number" class="form-control" name="phone" id="phone" placeholder="Mobile no.*">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="form-group">-->
<!--                            <input type="text" class="form-control" name="comapany" id="comapany" placeholder="Company name">-->
<!--                        </div>-->
<!--                        <div class="form-group">-->
<!--                            <textarea name="message" id="message" cols="30" rows="5" class="form-control" placeholder="Message"></textarea>-->
<!--                        </div>-->
<!--                        <div class="form-group text-left">-->
<!--                            <div class="captcha">-->
<!--                                <img src="--><?php //echo bloginfo('template_url'); ?><!--/assets/img/captcha.jpg">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="form-group text-left">-->
<!--                            <button type="button" class="btn btn-request">Request For Free Demo</button>-->
<!--                        </div>-->
<!--                    </div>-->
                    <?php echo do_shortcode('[contact-form-7 id="5" title="Get a Free Demo"]') ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="banner hidden-md">
    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/mobile-banner.jpg" class="main-banner">
    <div class="container banner-content-wrapp">
        <div class="row">
            <div class="col-12">
                <div class="banner-content-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h2>A Full Fledge <br>Fleet Management <br class="hidden-md">System</h2>
                    <p>Navigate with ease with Overdrive’s industry-leading vehicle <br>tracking system.  More than a car GPS tracker, we deliver all the <br>valuable data you need to get moving efficiently.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="form hidden-md">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="banner-form wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h4 class="text-left">Get a Free Demo</h4>
                    <div class="form-contact">
                        <div class="form-group">
                            <input type="text" class="form-control" name="fname" id="fname" placeholder="Name*">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email*">
                        </div>
                        <div class="form-group c-row">
                            <div class="c-col-1">
                                <select name="country" id="country" class="form-control">
                                    <option value="65">+65</option>
                                    <option value="84">+84</option>
                                </select>
                            </div>
                            <div class="c-col-2">
                                <input type="number" class="form-control" name="phone" id="phone" placeholder="Mobile no.*">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="comapany" id="comapany" placeholder="Company name">
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="message" cols="30" rows="5" class="form-control" placeholder="Message"></textarea>
                        </div>
                        <div class="form-group text-left">
                            <div class="captcha">
                                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/captcha.jpg">
                            </div>
                        </div>
                        <div class="form-group text-left">
                            <button type="button" class="btn btn-request">Request For Free Demo</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Banner -->

<!--About us-->
<section class="section-wrapp about-us">
    <div class="container">
        <div class="heading text-center mr-t-50 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
            <h3>More Than Just Wheels</h3>
            <p>OverWheels uses advanced telematics to provide real-time updates on where your vehicles are at all times. </p>
        </div>
        <div class="row mr-t-50">
            <div class="col-12 col-md-6 col-sm-12 order-md-12">
                <div class="key-higtlight-video wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <iframe width="100%" height="395" src="https://www.youtube.com/embed/jzeSULdtMaQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="key-highlight-video-title wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">OverWheels by Overdrive</div>
                <p class="video-desc wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">OverWheels is an ideal complement <br class="hidden-md">for companies operating in the <br class="hidden-md">delivery, logistics <br >and freight industries. </p>
                <hr class="hidden-md mb-hr">
            </div>
            <div class="col-12 col-sm-12 col-md-6 order-md-1">
                <div class="key-highlight-title wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">Key Highlights</div>
                <div class="key-highlight-list">
                    <div class="key-highlight-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="key-highlight-image text-center">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/key-item-1.jpg" alt="">
                        </div>
                        <div class="key-highlight-info">
                            <h4>Real-time Vehicle Telematics</h4>
                            <p>Through real time car tracking, you can pinpoint the <br class="hidden-xs">exact location of your fleet and get alerts on vehicle <br class="hidden-xs">status at all times.</p>
                        </div>
                    </div>
                    <div class="key-highlight-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="key-highlight-image text-center">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/key-item-2.jpg" alt="">
                        </div>
                        <div class="key-highlight-info">
                            <h4>Support Additonal Sensors</h4>
                            <p>From vehicle trackers to temperature <br class="hidden-md">and fuel <br class="hidden-xs">devices, our high powered <br class="hidden-md">sensors deliver unlimited <br class="hidden-xs">possibilities. <br class="hidden-md">We turn valuable insights into <br>remarkable breakthroughs.</p>
                        </div>
                    </div>
                    <div class="key-highlight-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="key-highlight-image text-center">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/key-item-3.jpg" alt="">
                        </div>
                        <div class="key-highlight-info">
                            <h4>Driver Behaviour</h4>
                            <p>Overdrive’s fleet management system <br class="hidden-md">enables you to <br class="hidden-xs">monitor unsafe or <br class="hidden-md">inappropriate driving behavior <br class="hidden-xs">and help <br class="hidden-md">minimise accident.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--End About us-->

<!--Key features-->
<section class="section-wrapp key-features hidden-xs">
    <div class="container">
        <div class="heading text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
            <h3>Empowering Key Features</h3>
            <p>Providing smart hardware and software you can trust.</p>
        </div>
        <div class="row mr-t-100 justify-content-md-center">
            <div class="col-12 col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <div class="key-feature-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                            <div class="key-feature-img">
                                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/key-f-1.png" alt="">
                            </div>
                            <div class="key-feature-info">
                                <h4>Real-time Tracking, <br>Alerts and Notifications</h4>
                                <p>Real-time data available at their <br>fingertips, you can respond with <br>greater speed and accuracy to your <br>business needs.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="key-feature-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                            <div class="key-feature-img">
                                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/key-f-2.png" alt="">
                            </div>
                            <div class="key-feature-info">
                                <h4>Easy Integration</h4>
                                <p>Reduce the time required to build a <br>market ready solution by using powerful <br>tools developed by Overdrive.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="key-feature-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                            <div class="key-feature-img">
                                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/key-f-3.png" alt="">
                            </div>
                            <div class="key-feature-info">
                                <h4>Improve Efficiency <br>and Productivity</h4>
                                <p>Overdrive platform allows unlimited <br>number of vehicle trackers and sensors <br>to interact with you, providing <br>real-time information.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="key-feature-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                            <div class="key-feature-img">
                                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/key-f-4.png" alt="">
                            </div>
                            <div class="key-feature-info">
                                <h4>Highly Robust & Scalable</h4>
                                <p>Overdrive platform allows unlimited <br>number of vehicle trackers and sensors <br>to interact with you, providing <br>real-time information.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="key-feature-item mr-b-0 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                            <div class="key-feature-img">
                                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/key-f-5.png" alt="">
                            </div>
                            <div class="key-feature-info">
                                <h4>Increase Profitability <br>and Save Cost</h4>
                                <p>Our techies do all the heavy lifting. You <br>focus on your strategy, your customers <br>and your market.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="key-feature-item mr-b-0 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                            <div class="key-feature-img">
                                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/key-f-6.png" alt="">
                            </div>
                            <div class="key-feature-info">
                                <h4>Detailed Analytics</h4>
                                <p>Our techies do all the heavy lifting. You <br>focus on your strategy, your customers <br>and your market.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Key features-->
<hr class="hidden-md mb-hr mr-tb-0 w-80">
<!--Solutions-->
<section class="section-wrapp solution">
    <div class="container">
        <div class="heading text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
            <h3>Solutions that Make Businesses Better</h3>
            <p class="lh-24 hidden-xs">Overdrive’s range of innovative solutions are driven by our expertise and desire to optimise the <br>efficiencies of businesses. We integrated key fleet features such as route optimization, GPS car tracking, and delivery tracking into our basic plan.</p>
            <p class="lh-24 hidden-md" style="width: 100%;">Overdrive’s range of innovative <br>solutions are driven by our expertise <br>and desire to optimise the <br>efficiencies of businesses – while <br>improving the lives of the people in <br>these companies, and their families.</p>
        </div>
        <div class="row mr-t-100 justify-content-md-center">
            <div class="col-12 col-md-10">
                <div class="solution-list">
                    <div class="hidden-xs">
                        <div class="row">
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-1.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>Covid-19 Solutions</h3>
                                        <div class="desc">A new innovative solution to help flatten the <br>curve! #Locator is a monitoring system <br>tailored to enhance social distancing and <br>ensure these measures are adhered to in the <br>most effective way.</div>
                                        <a href="https://overdriveiot.com/solutions/covid-19-solutions/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-2.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>Real Time Location System</h3>
                                        <div class="desc">Designed to address your requirements. <br>Ability to track the precise locations of people <br>and objects within buildings, facilities or <br>compounds – indoor and outdoor areas.</div>
                                        <a href="https://overdriveiot.com/solutions/real-time-location-system/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-3.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>School Bus System</h3>
                                        <div class="desc">Enables parents and schools to keep track of <br>their young explorers, whenever they travel to <br>and from school. Parents and schools will <br>never have to wonder where their children <br>are again!</div>
                                        <a href="https://overdriveiot.com/solutions/school-bus-system/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-4.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>Ridership Management</h3>
                                        <div class="desc">Allows companies to locate and track <br>employees, interact with bus operators, and <br>make supervisors’ jobs simpler with real-time <br>transport monitoring.</div>
                                        <a href="https://overdriveiot.com/solutions/ridership-management/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-5.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>Car Sharing Solution</h3>
                                        <div class="desc">A robust GPS-based fleet management <br>system that equips fleet owners with the tools <br>and software you need to gather, analyse and <br>utilise the data from reports generated by our <br>innovative tracking technology.</div>
                                        <a href="https://overdriveiot.com/solutions/car-sharing-solution/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-6.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>Fleet Management System</h3>
                                        <div class="desc">An efficient system to manage your fleet is <br>necessary to avoid wasteful overhead. Every <br>fleet requires the best solution to take <br>real-time control of your vehicles and prevent <br>all possible related risks.</div>
                                        <a href="https://overdriveiot.com/solutions/fleet-management-system/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden-md">
                        <div class="row">
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-6.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>Fleet Management System</h3>
                                        <div class="desc">An efficient system to manage your fleet <br>is necessary to avoid wasteful <br>overhead. Every fleet requires the best <br>solution to take real-time control of your <br>vehicles and prevent all possible related <br>risks.</div>
                                        <a href="https://overdriveiot.com/solutions/fleet-management-system/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-4.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>Employee Transportation <br>System</h3>
                                        <div class="desc">Allows companies to locate and track <br>employees, interact with bus operators, <br>and make supervisors’ jobs simpler with <br>real-time transport monitoring.</div>
                                        <a href="https://overdriveiot.com/solutions/ridership-management/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-5.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>Car Sharing Solution</h3>
                                        <div class="desc">A robust GPS-based fleet management <br>system that equips fleet owners with <br>the tools and software you need to <br>gather, analyse and utilise the data <br>from reports generated by our <br>innovative tracking technology.</div>
                                        <a href="https://overdriveiot.com/solutions/car-sharing-solution/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-1.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>Covid-19 Solutions</h3>
                                        <div class="desc">A new innovative solution to help flatten <br>the curve! #Locator is a monitoring <br>system tailored to enhance social <br>distancing and ensure these measures <br>are adhered to in the most effective <br>way.</div>
                                        <a href="https://overdriveiot.com/solutions/covid-19-solutions/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-2.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>Real Time Location System</h3>
                                        <div class="desc">Designed to address your requirements. <br>Ability to track the precise locations of <br>people and objects within buildings, <br>facilities or compounds – indoor and <br>outdoor areas.</div>
                                        <a href="https://overdriveiot.com/solutions/real-time-location-system/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 list-item">
                                <div class="solution-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                    <div class="solution-thumbnail">
                                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/solution-3.jpg" alt="">
                                    </div>
                                    <div class="solution-info">
                                        <h3>School Bus System</h3>
                                        <div class="desc">Enables parents and schools to keep <br>track of their young explorers, whenever <br>they travel to and from school. Parents <br>and schools will never have to wonder <br>where their children are again!</div>
                                        <a href="https://overdriveiot.com/solutions/school-bus-system/" class="more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!--End Solutions-->
<hr class="hidden-md mb-hr mr-tb-0 w-80">
<!--Partner-->
<section class="section-wrapp partner">
    <div class="container">
        <div class="heading text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
            <h3>Our Partners & Clients</h3>
            <p>Clients are our partners, they play a <br class="hidden-md">vital role in the creation of our <br class="hidden-md">world-class solutions.</p>
        </div>
        <div class="hidden-xs">
            <div class="row mr-t-45 justify-content-md-center partner-list">
                <div class="col-6 col-md-3">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/partner-1.png" alt="">
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/partner-2.png" alt="">
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/partner-3.png" alt="">
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/partner-4.png" alt="">
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/partner-5.png" alt="">
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/partner-6.png" alt="">
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/partner-7.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden-md">
            <div class="row mr-t-45 justify-content-md-center partner-list">
                <div class="col-6 col-md-3">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
                        <a href="">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/p-1.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <a href="">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/p-2.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                        <a href="">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/p-3.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                        <a href="">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/p-4.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
                        <a href="">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/p-5.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                        <a href="">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/p-6.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-8 col-md-4">
                    <div class="partner-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
                        <a href="">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/p-7.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!--End Partner-->

<!--Gain free-->
<section class="section-wrapp gain-free">
    <div class="container text-center">
        <h3>Gain free total visibility of your assets.</h3>
        <a href="javascript:;" id="req-form" class="btn btn-request">Request Free Demo</a>
    </div>
</section>
<!--End Gain free-->
<?php get_footer() ?>