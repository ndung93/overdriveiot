<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Overdrive | Home</title>
    <link rel="stylesheet" href="https://use.typekit.net/gys3ncu.css">
    <link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/assets/css/slick-theme.css">
    <link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/assets/css/slick.scss">
    <link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/assets/css/responsive.css">
    <script src="<?php echo bloginfo('template_url'); ?>/assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo bloginfo('template_url'); ?>/assets/js/jquery.nicescroll.js"></script>
    <script>
        var re_url = '<?php echo site_url('thank-you') ?>';
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KB5PJFG');</script>
    <!-- End Google Tag Manager -->

    <?php wp_head(); ?>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KB5PJFG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <!-- Header -->
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-5">
                    <div class="logo">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/logo.png" alt="">
                    </div>
                </div>
                <div class="col-6 col-md-7">
                    <div class="button_menu hidden-md">
                        <img width="25" src="<?php echo bloginfo('template_url') ?>/assets/img/menu.svg">
                    </div>
                    <div class="nav-bar hidden-xs">
                        <ul>
                            <li><a href="https://overdriveiot.com/solutions/">Solutions</a></li>
                            <li><a href="https://overdriveiot.com/products/">Products</a></li>
                            <li><a href="https://overdriveiot.com/knowledge/resources/">Resources</a></li>
                            <li><a href="https://overdriveiot.com/about/">About</a></li>
                            <li><a href="https://overdriveiot.com/contact/">Contact</a></li>
                            <li><a class="nav-btn" href="<?php echo site_url() ?>">Request Demo</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="mobile_menu hidden-md">
        <div class="mb-logo-center">
            <div class="logo">
                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/logo.png" alt="">
            </div>
        </div>
        <a href="javascrip:;" class="close-btn">
            <img src="<?php echo bloginfo('template_url') ?>/assets/img/cancel.svg">
        </a>
        <div class="mobile_inner">
            <ul id="nav_mobile" class="nav mb-navbar-nav cl-effect-11">
                <li><a href="https://overdriveiot.com/solutions/">Solutions</a></li>
                <li><a href="https://overdriveiot.com/products/">Products</a></li>
                <li><a href="https://overdriveiot.com/knowledge/resources/">Resources</a></li>
                <li><a href="https://overdriveiot.com/about/">About</a></li>
                <li><a href="https://overdriveiot.com/contact/">Contact</a></li>
            </ul>
            <a class="nav-btn" href="<?php echo site_url() ?>">Request Demo</a>
        </div>
    </div>
    <div class="mobile-overplay"></div>
    <!-- End Header -->