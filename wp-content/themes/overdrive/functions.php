<?php
require_once('wp_bootstrap_navwalker.php');
if (!session_id()) {
    session_set_cookie_params('846000000000'); // 1 hour
    session_start();
}

if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    add_theme_support( 'post-formats', array( 'video', 'gallery' ) );
}

add_image_size('thumb90_77', 90, 77, array( 'center', 'center' ));
add_image_size('thumb175_150', 175, 150, array( 'center', 'center' ));
add_image_size('thumb447_245', 447, 245, array( 'center', 'center' ));
add_image_size('thumb330_220', 330, 230, array( 'center', 'center' ));
add_image_size('thumb335_206', 335, 206, array( 'center', 'center' ));
add_image_size('thumb340_200', 340, 200, array( 'center', 'center' ));
add_image_size('thumb695_212', 695, 212, array( 'center', 'center' ));
add_image_size('thumb338_212', 338, 212, array( 'center', 'center' ));
add_image_size('thumb344_212', 344, 212, array( 'center', 'center' ));
add_image_size('thumb338_443', 338, 443, array( 'center', 'center' ));
add_image_size('thumb537_290', 537, 290, array( 'center', 'center' ));
add_image_size('thumb935_510', 935, 510, array( 'center', 'center' ));
add_image_size('thumb220_130', 220, 130, array( 'center', 'center' ));
add_image_size('thumb380_230', 380, 230, array( 'center', 'center' ));

register_nav_menus(array(
    'primary' => __('Primary Menu', 'diaocnhontrach'),
    'footer_1' => __('Hướng dẫn', 'diaocnhontrach'),
    'footer_2' => __('Quy định', 'diaocnhontrach')
));


function hui_excerpt_length($length) {
    return 50;
}
add_filter('excerpt_length', 'hui_excerpt_length');

function hui_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'hui_excerpt_more');

function register_beess_widget() {
    register_sidebar(array(
        'name' => 'Right Sidebar',
        'id' => 'right-sidebar',
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
	    'after_widget' => '</div>',
	    'before_title' => '<h3 class="widget-title">',
	    'after_title' => '</h3>',
    ));
}
add_action( 'widgets_init', 'register_beess_widget' );

add_action ( 'admin_enqueue_scripts', function () {
    if (is_admin ())
        wp_enqueue_media();
});

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }
    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content); 
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

function hui_cptui_add_post_type_to_search( $query ) {
	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}
	if ( $query->is_search() ) {
		$query->set(
			'post_type',
			array( 'post', 'products', 'solutions', 'projects')
		);
	}
}
add_filter( 'pre_get_posts', 'hui_cptui_add_post_type_to_search' );

function _breadcrumbs() {
    $delimiter = '';
    $home = 'Trang chủ';
    $before = '<li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">';
    $after = '</li>';
    if ( !is_home() && !is_front_page() || is_paged() ) {
        echo '<ol class="breadcrumb">';
            global $post;
            $homeLink = get_bloginfo('url');
            echo $before.'<a href="' . $homeLink . '">' . $home . '</a> '. $after . $delimiter . ' ';
            if ( is_category() ) {
                global $wp_query;
                $cat_obj = $wp_query->get_queried_object();
                $thisCat = $cat_obj->term_id;
                $thisCat = get_category($thisCat);
                $parentCat = get_category($thisCat->parent);
                if ($thisCat->parent != 0) echo $before . (get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' ')) . $after;
                echo $before . single_cat_title('', false) . $after;
            } elseif ( is_day() ) {
                echo $before .'<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> '. $after . $delimiter . ' ';
                echo $before .'<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> '. $after . $delimiter . ' ';
                echo $before . get_the_time('d') . $after;
            } elseif ( is_month() ) {
                echo $before .'<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> '. $after . $delimiter . ' ';
                echo $before . get_the_time('F') . $after;
            }  elseif ( is_year() ) {
                echo $before . get_the_time('Y') . $after;
            } elseif ( is_single() && !is_attachment() ) {
                if ( get_post_type() != 'post' ) {
                    $post_type = get_post_type( get_the_ID() );
                    if($post_type == 'listings'){
                        echo $before .'<a href="' . $homeLink . '/nha-dat-dang-ban/">Nhà đất đang bán</a> '. $after . ' ';
                    } elseif($post_type == 'consultant'){
                        echo $before .'<a href="' . $homeLink . '/du-an-tu-van/">Tư vấn</a> '. $after . ' ';
                    }
                    echo $before . get_the_title() . $after;
                } else {
                    $cat = get_the_category();
                    $cat = $cat[0];
                    echo $before . get_category_parents($cat, TRUE, ' ' . $delimiter . ' '). $after;
                    echo $before . get_the_title() . $after;
                }
                
            } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
                $post_type = get_post_type_object(get_post_type());
                if(is_tax()){
                    $taxonomy = get_queried_object();
                    if($taxonomy->parent != 0){
                        $term = get_term( $taxonomy->parent );
                        echo $before .'<a href="' . $homeLink . '/' . $term->slug . '/">' . $term->name . '</a> '. $after . ' ';
                    }
                    echo $before . $taxonomy->name . $after;
                }else{
                    echo $before . $post_type->labels->singular_name . $after .$delimiter;
                }
            } elseif ( is_attachment() ) {
                $parent = get_post($post->post_parent);
                $cat = get_the_category($parent->ID); $cat = $cat[0];
                echo $before . get_category_parents($cat, TRUE, ' ' . $delimiter . ' ') . $after;
                echo $before .'<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> '. $after . $delimiter . ' ';
                echo $before . get_the_title() . $after;
            } elseif ( is_page() && !$post->post_parent ) {
                echo $before . get_the_title() . $after;
            } elseif ( is_page() && $post->post_parent ) {
                $parent_id = $post->post_parent;
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_page($parent_id);
                    $breadcrumbs[] = $before .'<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>'. $after;
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
                echo $before . get_the_title() . $after;
            } elseif ( is_search() ) {
                echo $before . 'Tìm kiếm "' . get_search_query() . '"' . $after;
            } elseif ( is_tag() ) {
                echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
            } elseif ( is_author() ) {
                global $author;
                echo $before . 'Articles posted by ' . $userdata->display_name . $after;
            } elseif ( is_404() ) {
                echo $before . 'Error 404' . $after;
            }
            if ( get_query_var('paged') ) {
                if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
                echo __('Page') . ' ' . get_query_var('paged');
                if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
            }
        echo '</ol>';
    }
}

function get_meta_slider_home($slider_id) {
    $args = array(
        'force_no_custom_order' => true,
        'orderby' => 'menu_order',
        'order' => 'DESC',
        'post_type' => array('attachment', 'ml-slide'),
        'post_status' => array('inherit', 'publish'),
        'tax_query' => array(
            array(
                'taxonomy' => 'ml-slider',
                'field' => 'slug',
                'terms' => $slider_id
            )
        )
    );
    wp_reset_query();
    query_posts($args);
    while (have_posts()): the_post();
        $metas = get_post_meta(get_the_ID(), 'ml-slider_url');
        $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
        ?>
        <div class="banner-slider__item">
            <img src="<?php echo $featured_img_url ?>" alt="">
            <div class="share-banner__button" >   <div class="fb-share-button" data-href="<?php echo site_url() ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="<?php echo site_url() ?>" class="fb-xfbml-parse-ignore">Chia sẻ</a></div></div>
        </div>
        <?php
    endwhile;
    wp_reset_query();
}

function get_post_view() {
    $count = get_post_meta( get_the_ID(), 'post_views_count', true );
    return "$count views";
}
function set_post_view() {
    $key = 'post_views_count';
    $post_id = get_the_ID();
    $count = (int) get_post_meta( $post_id, $key, true );
    $count++;
    update_post_meta( $post_id, $key, $count );
}
function posts_column_views( $columns ) {
    $columns['post_views'] = 'Views';
    return $columns;
}
function posts_custom_column_views( $column ) {
    if ( $column === 'post_views') {
        echo get_post_view();
    }
}
add_filter( 'manage_posts_columns', 'posts_column_views' );
add_action( 'manage_posts_custom_column', 'posts_custom_column_views' );

function _pagination() {
    if (is_singular())
        return;
    global $wp_query;
    /** Ngừng thực thi nếu có ít hơn hoặc chỉ có 1 bài viết */
    if ($wp_query->max_num_pages <= 1)
        return;
    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max = intval($wp_query->max_num_pages);

    /** Thêm page đang được lựa chọn vào mảng*/
    if ($paged >= 1)
        $links[] = $paged;
    /** Thêm những trang khác xung quanh page được chọn vào mảng */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (($paged + 2) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    /** Hiển thị thẻ đầu tiên \n để xuống dòng code */
    echo '<ul class="pagination justify-content-center">'.
    "\n";

    /** Hiển thị link về trang trước */
    if (get_previous_posts_link())
        printf('<li class="page-item">%s</li>'.
            "\n", get_previous_posts_link('<span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span>'));

    /** Nếu đang ở trang 1 thì nó sẽ hiển thị đoạn này */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' active' : '';
        printf('<li class="page-item" ><a rel="nofollow" class="page-link %s" href="%s">%s</a></li>'.
            "\n", $class, esc_url(get_pagenum_link(1)), '1');
        if (!in_array(2, $links))
            echo '<li>…</li>';
    }

    /** Hiển thị khi đang ở một trang nào đó đang được lựa chọn */
    sort($links);
    foreach((array) $links as $link) {
        $class = $paged == $link ? ' active' : '';
        printf('<li class="page-item"><a rel="nofollow" class="page-link %s" href="%s">%s</a></li>'.
            "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /** Hiển thị khi đang ở trang cuối cùng */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links))
            echo '<li>…</li>'.
        "\n";
        $class = $paged == $max ? ' active' : '';
        printf('<li class="page-item"><a rel="nofollow" class="page-link %s" href="%s">%s</a></li>'.
            "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    /** Hiển thị link về trang sau */
    if (get_next_posts_link())
        printf('<li class="page-item">%s</li>'.
            "\n", get_next_posts_link('<span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span>'));
    echo '</ul>'.
    "\n";
}

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="page-link"';
}

function main_slider() {
    $args = array(
        'orderby' => 'menu_order',
        'order' => 'DESC',
        'post_type' => 'banner',
    );
    ?>
    <div class="main-slider">
        <?php
        wp_reset_query();
        query_posts($args);
        while (have_posts()): the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
            $main_banner = get_field_object('banner_position', get_the_ID());
            $url = get_field('banner_link', get_the_ID());
            $mb_main_banner = get_field('mobile_image', get_the_ID());
            $value = $main_banner['value'];
            if($value == 'main') :
            ?>
                <div>
                    <div class="sl-caption"><a class="slider-link" href="<?php echo $url; ?>"></a></div>
                    <img class="d-none d-sm-block d-md-block d-lg-block d-xl-block" src="<?php echo $featured_img_url; ?>">
                    <img class="d-block d-sm-none d-md-none d-lg-none d-xl-none" src="<?php echo $mb_main_banner; ?>">
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    </div>
    <?php
}

function float_banner() {
    $args = array(
        'orderby' => 'menu_order',
        'order' => 'DESC',
        'post_type' => 'banner',
    );
    query_posts($args);
    $max = 0;
    while (have_posts()): the_post();
        $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
        $url = get_field('banner_link', get_the_ID());
        $main_banner = get_field_object('banner_position', get_the_ID());
        $value = $main_banner['value'];
        if($value == 'float') :
            $max++;
            if($max == 1) :
            ?>
                <div class="float-banner banner-left" data-sticky data-margin-top="50">
                    <a href="<?php echo $url; ?>" target="_blank">
                        <img src="<?php echo $featured_img_url; ?>" class="float-left" alt="Sticky">
                    </a>
                </div>
            <?php
            endif;
            if($max == 2) :
            ?>
                <div class="float-banner banner-right" data-sticky data-margin-top="50">
                    <a href="<?php echo $url; ?>" target="_blank">
                        <img src="<?php echo $featured_img_url; ?>" class="float-right" alt="Sticky">
                    </a>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    <?php 
    endwhile;
    wp_reset_query();
    ?>
    <?php
}

function my_acf_admin_head() {
    ?>
    <style type="text/css">
        .propertya_attributes .acf-fields.-left>.acf-field>.acf-label,
        .propertya_attributes .acf-fields.-left>.acf-field:before {
            width: 40%;
        }
        .propertya_attributes .acf-fields.-left>.acf-field>.acf-input{
            width: 60%;
        }
        .propertya_gallery .acf-gallery{
            height:354px !important;
        }
    </style>
    <?php
}
add_action('acf/input/admin_head', 'my_acf_admin_head');

function _tags(){
    $post_tags = get_the_tags(get_the_ID());
    if (!empty($post_tags)) {
        $output = '<div class="keyword"><h4>Từ khóa tìm kiếm</h4>';
        foreach ($post_tags as $tag) {
            $output .= '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>';
        }
        $output .= '</div>';
        echo $output;
    }
}

// tax_count
function tax_count ( $taxonomy, $term, $post_type ) {
    $args = array(
        'post_type' =>  $post_type,
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => $term,
            ),
        ),
    );
    $tax_query = new WP_Query( $args );
    return $tax_query->found_posts;
    wp_reset_query();
}

function viewedPost(){
	session_start();
	if(!isset($_SESSION["viewed"])){
		$_SESSION["viewed"] = array();
	}
	if(is_singular()){
		$_SESSION["viewed"][get_the_ID()] = get_the_ID();
	}
}
add_action('wp', 'viewedPost');

add_action( 'wp_ajax_get_listing_feature', 'get_listing_feature_init' );
add_action( 'wp_ajax_nopriv_get_listing_feature', 'get_listing_feature_init' );
function get_listing_feature_init() {
    global $wpdb;
    $html = '';
    $get_total = wp_get_recent_posts(array(
        'numberposts' => -1,
        'post_type'   => 'listings',
        'post_status' => 'publish',
        'meta_key' => '_is_ns_featured_post',
        'meta_value' => 'yes',
        'post_status' => array('inherit', 'publish'),
    ));
    $total = ($get_total) ? count($get_total) : 0 ;
    $recent_posts = wp_get_recent_posts(array(
        'numberposts' => 8,
        'offset' => 4,
        'post_type'   => 'listings',
        'post_status' => 'publish',
        'meta_key' => '_is_ns_featured_post',
        'meta_value' => 'yes',
        'post_status' => array('inherit', 'publish'),
    ));

    if ($recent_posts) :
        foreach ($recent_posts as $post) :
            $attr_group = get_field('propertya_attributes', $post['ID']);
            if($attr_group['property_total_price']){
                $price = ($attr_group['property_total_price'] > 1000) ? ($attr_group['property_total_price']/1000).' tỷ' : $attr_group['property_total_price'].' triệu';
            }else{
                $price = "Thỏa thuận";
            }
            $html .= '<div class="col-12 col-md-3">
                        <div class="listing-item item-shadow-5px fix-h415">
                            <div class="listing-item-thumb img-hover">
                                <a href="'.get_permalink($post['ID']).'">
                                    '.get_the_post_thumbnail($post['ID'], 'thumb340_200', array( 'title' => $post['post_title'], 'class' => 'd-none d-sm-block d-md-block d-lg-block d-xl-block' )).'
                                    '.get_the_post_thumbnail($post['ID'], 'full', array( 'title' => $post['post_title'],'class' => 'd-block d-sm-none d-md-none d-lg-none d-xl-none' )).'
                                </a>
                            </div>
                            <div class="listing-item-info">
                                <h3><a href="'.get_permalink($post['ID']).'">'.wp_trim_words( $post['post_title'], 20, '...' ).'</a></h3>
                                <p class="item-info-area"><strong>'.$price.'</strong>';
                                if($attr_group['property_area']){
                                    $html .= '<span class="dot"></span> <strong>'.$attr_group['property_area'].'m²</strong></p>';
                                }
                                if($attr_group['property_address']){
                                    $html .= '<p class="item-info-address">'.$attr_group['property_address'].'</p>';
                                }
                                $html .= '<hr>
                                            <div class="item-footer">
                                                <p class="item-info-date">'.get_the_date( 'd-m-Y', $post['ID'] ).'</p>
                                            </div>
                            </div>
                        </div>
                    </div>';
        endforeach; wp_reset_query();
        $success = 1;
    else:
        $success = 0;
    endif;

    echo json_encode(array('success' => $success, 'html' => $html, 'total' => $total));
    die();
}

add_action( 'wp_ajax_get_listing', 'get_listing_init' );
add_action( 'wp_ajax_nopriv_get_listing', 'get_listing_init' );
function get_listing_init() {
    global $wpdb;
    $html = '';
    $get_total = wp_get_recent_posts(array(
        'numberposts' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
        'post_type'   => 'listings',
        'post_status' => 'publish',
        'post_status' => array('inherit', 'publish'),
    ));
    $total = ($get_total) ? count($get_total) : 0 ;
    $recent_posts = wp_get_recent_posts(array(
        'numberposts' => 8,
        'offset' => 4,
        'orderby' => 'date',
        'order' => 'DESC',
        'post_type'   => 'listings',
        'post_status' => 'publish',
        'post_status' => array('inherit', 'publish'),
    ));

    if ($recent_posts) :
        foreach ($recent_posts as $post) :
            $attr_group = get_field('propertya_attributes', $post['ID']);
            if($attr_group['property_total_price']){
                $price = ($attr_group['property_total_price'] > 1000) ? ($attr_group['property_total_price']/1000).' tỷ' : $attr_group['property_total_price'].' triệu';
            }else{
                $price = "Thỏa thuận";
            }
            $html .= '<div class="col-12 col-md-3">
                        <div class="listing-item item-shadow-5px fix-h415">
                            <div class="listing-item-thumb img-hover">
                                <a href="'.get_permalink($post['ID']).'">
                                    '.get_the_post_thumbnail($post['ID'], 'thumb340_200', array( 'title' => $post['post_title'], 'class' => 'd-none d-sm-block d-md-block d-lg-block d-xl-block' )).'
                                    '.get_the_post_thumbnail($post['ID'], 'full', array( 'title' => $post['post_title'],'class' => 'd-block d-sm-none d-md-none d-lg-none d-xl-none' )).'
                                </a>
                            </div>
                            <div class="listing-item-info">
                                <h3><a href="'.get_permalink($post['ID']).'">'.wp_trim_words( $post['post_title'], 20, '...' ).'</a></h3>
                                <p class="item-info-area"><strong>'.$price.'</strong>';
                                if($attr_group['property_area']){
                                    $html .= '<span class="dot"></span> <strong>'.$attr_group['property_area'].'m²</strong></p>';
                                }
                                if($attr_group['property_address']){
                                    $html .= '<p class="item-info-address">'.$attr_group['property_address'].'</p>';
                                }
                                $html .= '<hr>
                                            <div class="item-footer">
                                                <p class="item-info-date">'.get_the_date( 'd-m-Y', $post['ID'] ).'</p>
                                            </div>
                            </div>
                        </div>
                    </div>';
        endforeach; wp_reset_query();
        $success = 1;
    else:
        $success = 0;
    endif;

    echo json_encode(array('success' => $success, 'html' => $html, 'total' => $total));
    die();
}

// add_filter( 'wp_enqueue_scripts', 'change_default_jquery', PHP_INT_MAX );

// function change_default_jquery( ){
//     wp_dequeue_script( 'jquery');
//     wp_deregister_script( 'jquery');   
// }
//ẩn ACF va CPT UI menu admin
//add_filter('acf/settings/show_admin', '__return_false');
//remove_action( 'admin_menu', 'cptui_plugin_menu' );
//
add_filter('wpcf7_spam', '__return_false');