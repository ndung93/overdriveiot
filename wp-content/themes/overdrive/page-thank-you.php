<?php get_header('thank'); ?>
<section class="thk-content">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="thk-entry-content">
                    <h3>Thank you for contacting us!</h3>
                    <p>We got your email address and our consultant will be in touch with you shortly. <br>If you have any questions, do call us at <a href="tel:+6569500890">+65 6950 0890</a>.</p>
                    <p>Meantime, do follow us on <a href="https://www.facebook.com/overdrivenow" target="_blank">Facebook</a> or <a href="https://www.linkedin.com/company/overdrive-iot-pte-ltd"  target="_blank">LinkedIn</a> for our latest news and launches.</p>
                    <a href="/wp-content/uploads/2019/09/OverdriveCompanyProfile_20190917.pdf" class="btn btn-download" target="_blank">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/icon-doc.png"> Download Overdrive Brochure
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-5">
                <div class="thk-img text-center hidden-xs">
                    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/thk-img.jpg">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact">
    <div class="contact-wrap">
        <div class="left-maps">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6708.383919495177!2d103.79411022408281!3d1.2760006337858945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da1be3666fdc0b%3A0x5bd305b815c003ee!2sOverdrive%20IOT%20Pte%20Ltd!5e0!3m2!1svi!2s!4v1612774553351!5m2!1svi!2s" width="100%" height="629" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
        <div class="right-contact">
            <div class="contact-item">
                <h2>SINGAPORE</h2>
                <p class="name">Overdrive IOT Pte Ltd</p>
                <p>100E Pasir Panjang Road <br>#04-01 Century Warehouse <br>Singapore 118521</p>
                <p class="phone">+6950 0890</p>
            </div>
            <div class="contact-item">
                <h2>MALAYSIA</h2>
                <p class="name">Overdrive IOT Sdn Bhd</p>
                <p>51C, Jalan Panglima <br>30000 Ipoh, Perak</p>
            </div>
            <div class="contact-item">
                <h2>BELARUS</h2>
                <p class="name">Overdrive IOT</p>
                <p>LLC “Lab 42” <br>90 Kharkovskaya str, <br>Minsk, Republic of Belarus, 220015</p>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>