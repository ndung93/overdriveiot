<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Overdrive | Home</title>
    <link rel="preload" as="font" href="https://use.typekit.net/gys3ncu.css">
    <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="<?php echo bloginfo('template_url'); ?>/assets/css/bootstrap.min.css">
    <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="<?php echo bloginfo('template_url'); ?>/assets/css/animate.css">
    <link rel="preload" as="font" onload="this.onload=null;this.rel='stylesheet'" href="<?php echo bloginfo('template_url'); ?>/assets/css/font-awesome.min.css">
    <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="<?php echo bloginfo('template_url'); ?>/assets/css/slick-theme.css">
    <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="<?php echo bloginfo('template_url'); ?>/assets/css/slick.scss">
    <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="<?php echo bloginfo('template_url'); ?>/assets/css/styles.css">
    <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="<?php echo bloginfo('template_url'); ?>/assets/css/responsive.css">
    <link rel="preload" as="image" href="<?php echo bloginfo('template_url'); ?>/assets/img/mobile-banner.jpg" />
    <script defer src="<?php echo bloginfo('template_url'); ?>/assets/js/jquery.nicescroll.js"></script>
    <script>
        var re_url = '<?php echo site_url('thank-you') ?>';
    </script>
    <!-- Google Tag Manager --> 
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KB5PJFG');</script>
    <!-- End Google Tag Manager -->

    <?php wp_head(); ?>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KB5PJFG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <!-- Header -->
    <header id="header">
        <div class="container">
            <div class="logo">
                <img src="<?php echo bloginfo('template_url'); ?>/assets/img/logo.png" alt="">
            </div>
        </div>
    </header>
    <!-- End Header -->